/* 
  $ g++ createDB.cpp -l sqlite3
*/

#include <iostream>
#include <stdlib.h>
#include <sqlite3.h>


static int callback(void *NotUsed, int argc, char **argv, char **azColName) {
   int i;
   for(i = 0; i<argc; i++) {
      printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
   }
   printf("\n");
   return 0;
}


int main(int argc, char** argv)
{
    sqlite3* db;
    const char *tablename = "EMPLOYEES";
    int rc = 0;
    const char *sql;
    char *zErrMsg = 0;

    
    /* open database */
    rc = sqlite3_open("/sylvanixhome/data/Evil-Inc.db", &db);
    
    if (rc) {
        std::cerr << "Error opening the database." << sqlite3_errmsg(db) << std::endl;
        return (-1);
    }
    else
        std::cout << "Database opened successfully." << std::endl;


    
    // does the table exist already?
    sql = "SELECT name FROM sqlite_master WHERE type='table' AND name='EMPLOYEES';";

    // return 0 if table does not exist, 1 if it does
    rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);

    // Create the table if it does not already exist
    if (! rc) {
 
        /* Create SQL statement */
        sql = "CREATE TABLE EMPLOYEES("  \
           "ID INT PRIMARY KEY     NOT NULL," \
           "NAME           TEXT    NOT NULL," \
           "AGE            INT     NOT NULL," \
           "ADDRESS        CHAR(50)," \
           "SALARY         REAL );";
        
        /* Execute SQL statement */
        rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);
        
        if( rc != SQLITE_OK ){
           fprintf(stderr, "SQL error: %s\n", zErrMsg);
           sqlite3_free(zErrMsg);
        } else {
           fprintf(stdout, "Table created successfully\n");
        }
    }



    // Insert Rows
    sql = "INSERT INTO EMPLOYEES (ID,NAME,AGE,ADDRESS,SALARY) " \
              "VALUES (8, 'Linus', 36, 'California', 30000.00 );" \
          "INSERT INTO EMPLOYEES (ID,NAME,AGE,ADDRESS,SALARY) " \
              "VALUES (9, 'Linus', 52, 'Oregon', 40000.00 );";
    
    /* Execute SQL statement */
    rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);
    
    if( rc != SQLITE_OK ){
       fprintf(stderr, "SQL error: %s\n", zErrMsg);
       sqlite3_free(zErrMsg);
    } else {
       fprintf(stdout, "Records created successfully\n");
    }


    
    /* close database */
    sqlite3_close(db);
    return (0);
}
