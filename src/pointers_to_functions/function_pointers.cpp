/*
    Show three different ways to use pointers to functions
    (even though such things are not often useful)
*/

#include <iostream>

void printGreeting();

int main()
{
    // 1st way
    void(*greeting1)() = printGreeting;
    greeting1();

    // 2nd way, use auto
    auto greeting2 = printGreeting;
    greeting2();

    // 3rd way, typedef
    typedef void(*thirdWay)();
    thirdWay greeting3 = printGreeting;
    greeting3();
}


void printGreeting() {
    std::cout << "Greetings human." << std::endl;
}
