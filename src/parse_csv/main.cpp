#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include "utils.hpp" // split
    

int main()
{
    std::string line;
    std::ifstream csvfile ("/sylvanixhome/data/planets.csv");
    if (csvfile.is_open()) {
        while ( getline (csvfile,line) ) {
            std::vector vline = split(line, ",");
            for (
                std::vector<std::string_view>::iterator it=vline.begin();
                it!=vline.end();
                ++it
            )
                std::cout << *it << "|";
            std::cout << '\n';
        }
        csvfile.close();
    }
    else std::cout << "Unable to open file" << '\n';
}
