#ifndef _UTILS_HPP__
#define _UTILS_HPP__

#include <iostream>
#include <string>
#include <vector>
#include <ranges>
    
/*
  Splits an input string on a delimiter and returns a vector of the sub-strings.
*/
template<typename T = std::string_view>
requires std::same_as<T, std::string> || std::same_as<T, std::string_view>
auto split(const std::string_view str, const std::string_view delim = ",") {
    std::vector<T> output;
    for (const auto& s : str | std::views::split(delim)) {
        const std::string_view sv { s.begin(), s.end() };
        output.emplace_back(
            std::ranges::find_if_not(sv, isspace),
            std::ranges::find_if_not(sv | std::views::reverse, isspace).base()
        );
    }
    return output;
}


#endif // _UTILS_HPP__
