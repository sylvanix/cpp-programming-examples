/*
    String Trimming with regular expressions
    Strip whitespace from the start and end of a string.
    (see https://stackoverflow.com/questions/25829143/trim-whitespace-from-a-string)
*/

#include <iostream>
#include <string>
#include <regex>

using std::cout;
using std::string;


string trim(string s);


int main()
{
    string input = "     Subterranian Homesick Blues  ";
    cout << "|" << trim(input) << "|" << '\n';
}


std::string trim(string s) {
    std::regex e("^\\s+|\\s+$"); // remove leading and trailing spaces
    return std::regex_replace(s, e, "");
}
